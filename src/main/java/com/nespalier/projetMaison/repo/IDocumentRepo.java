package com.nespalier.projetMaison.repo;

import com.nespalier.projetMaison.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDocumentRepo extends JpaRepository<Document, Long> {
}
