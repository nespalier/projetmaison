package com.nespalier.projetMaison.repo;

import com.nespalier.projetMaison.model.Bien;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IBienRepo extends JpaRepository<Bien, Long> {
}
