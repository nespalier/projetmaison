package com.nespalier.projetMaison.repo;

import com.nespalier.projetMaison.model.Prestataire;
import com.nespalier.projetMaison.model.Statut;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IStatutRepo extends JpaRepository<Statut, Long> {

}
