package com.nespalier.projetMaison.repo;

import com.nespalier.projetMaison.model.Bien;
import com.nespalier.projetMaison.model.BienPrestataire;
import com.nespalier.projetMaison.model.Prestataire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IBienPrestataireRepo extends JpaRepository<BienPrestataire, Long> {
}
