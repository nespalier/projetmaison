package com.nespalier.projetMaison.repo;

import com.nespalier.projetMaison.model.Bien;
import com.nespalier.projetMaison.model.TypePrestataire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITypePrestataireRepo extends JpaRepository<TypePrestataire, Long> {
}
