package com.nespalier.projetMaison.repo;

import com.nespalier.projetMaison.model.Bien;
import com.nespalier.projetMaison.model.TypeDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITypeDocumentRepo extends JpaRepository<TypeDocument, Long> {
}
