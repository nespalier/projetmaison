package com.nespalier.projetMaison.service;

import com.nespalier.projetMaison.commons.interfaces.BaseCrud;
import com.nespalier.projetMaison.model.Document;
import com.nespalier.projetMaison.model.Prestataire;

public interface IPrestataireService extends BaseCrud<Prestataire> {
}
