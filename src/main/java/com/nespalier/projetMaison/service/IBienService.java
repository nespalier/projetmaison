package com.nespalier.projetMaison.service;

import com.nespalier.projetMaison.commons.interfaces.BaseCrud;
import com.nespalier.projetMaison.model.Bien;

public interface IBienService  extends BaseCrud<Bien> {

}
