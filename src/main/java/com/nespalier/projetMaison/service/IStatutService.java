package com.nespalier.projetMaison.service;

import com.nespalier.projetMaison.commons.interfaces.BaseCrud;
import com.nespalier.projetMaison.model.Prestataire;
import com.nespalier.projetMaison.model.Statut;

public interface IStatutService extends BaseCrud<Statut> {
}
