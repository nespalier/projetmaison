package com.nespalier.projetMaison.service;

import com.nespalier.projetMaison.commons.interfaces.BaseCrud;
import com.nespalier.projetMaison.model.Document;
import com.nespalier.projetMaison.model.TypePrestataire;

public interface ITypePrestataireService extends BaseCrud<TypePrestataire> {
}
