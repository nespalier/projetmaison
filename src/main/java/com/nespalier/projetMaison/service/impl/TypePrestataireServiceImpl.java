package com.nespalier.projetMaison.service.impl;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.model.TypeDocument;
import com.nespalier.projetMaison.model.TypePrestataire;
import com.nespalier.projetMaison.repo.ITypeDocumentRepo;
import com.nespalier.projetMaison.repo.ITypePrestataireRepo;
import com.nespalier.projetMaison.service.ITypePrestataireService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TypePrestataireServiceImpl implements ITypePrestataireService {
    @Autowired
    ITypePrestataireRepo typePrestaRepo;

    TypePrestataire typePrestataire = new TypePrestataire();
    @Override
    public TypePrestataire creerModifier(TypePrestataire objetAcreer) throws ProjetMaisonExceptions {

        if (StringUtils.isNotEmpty(objetAcreer.getLibelle())) {
            typePrestataire = typePrestaRepo.save(objetAcreer);
        } else {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.LIBELLE_TYPE_PRESTATAIRE_VIDE);
        }
        return typePrestataire;
    }

    @Override
    public TypePrestataire trouveParClef(Long id) throws ProjetMaisonExceptions {

        Optional<TypePrestataire> op = typePrestaRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.TYPE_PRESTATIRE_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.TYPE_PRESTATIRE_INCONNU);
        } else {
            typePrestataire = typePrestaRepo.findById(id).get();
        }

        return typePrestataire;
    }

    @Override
    public List<TypePrestataire> chercherTousLesElements() {

        return typePrestaRepo.findAll(Sort.by(Sort.Direction.ASC, "libelle"));
    }

    @Override
    public void supprimerParId(Long id) throws ProjetMaisonExceptions {
        Optional<TypePrestataire> op = typePrestaRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.TYPE_PRESTATIRE_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.TYPE_PRESTATIRE_INCONNU);
        } else {
            typePrestaRepo.deleteById(id);
        }
    }
}
