package com.nespalier.projetMaison.service.impl;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.model.Document;
import com.nespalier.projetMaison.model.Prestataire;
import com.nespalier.projetMaison.repo.IDocumentRepo;
import com.nespalier.projetMaison.repo.IPrestataireRepo;
import com.nespalier.projetMaison.service.IPrestataireService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PrestataireServiceImpl implements IPrestataireService {
    @Autowired
    IPrestataireRepo prestataireRepo;

    Prestataire prestataire = new Prestataire();

    @Override
    public Prestataire creerModifier(Prestataire objetAcreer) throws ProjetMaisonExceptions {
        if (ObjectUtils.isNotEmpty(objetAcreer)) {
            prestataire = prestataireRepo.save(objetAcreer);
        }else {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.PRESTATAIRE_VIDE);
        }
        return prestataire;
    }

    @Override
    public Prestataire trouveParClef(Long id) throws ProjetMaisonExceptions {
        Optional<Prestataire> op = prestataireRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.PRESTATAIRE_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.PRESTATAIRE_INCONNU);
        } else {
            prestataire = prestataireRepo.findById(id).get();
        }

        return prestataire;
    }

    @Override
    public List<Prestataire> chercherTousLesElements() {

        return prestataireRepo.findAll(Sort.by(Sort.Direction.ASC, "type"));
    }

    @Override
    public void supprimerParId(Long id) throws ProjetMaisonExceptions {
        Optional<Prestataire> op = prestataireRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.PRESTATAIRE_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.PRESTATAIRE_INCONNU);
        } else {
            prestataireRepo.deleteById(id);
        }
    }
}
