package com.nespalier.projetMaison.service.impl;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.model.Bien;
import com.nespalier.projetMaison.model.BienPrestataire;
import com.nespalier.projetMaison.repo.IBienPrestataireRepo;
import com.nespalier.projetMaison.service.IBienPrestataireService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BienPrestataireServiceImpl implements IBienPrestataireService {

    @Autowired
    IBienPrestataireRepo bienPrestataireRepo;

    BienPrestataire bienPrestataire = new BienPrestataire();

    @Override
    public BienPrestataire creerModifier(BienPrestataire objetAcreer) throws ProjetMaisonExceptions {
        if (ObjectUtils.isNotEmpty(objetAcreer)) {
            bienPrestataire = bienPrestataireRepo.save(objetAcreer);
        } else {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_VIDE);
        }
        return bienPrestataire;
    }

    @Override
    public BienPrestataire trouveParClef(Long id) throws ProjetMaisonExceptions {
        Optional<BienPrestataire> op = bienPrestataireRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_INCONNU);
        } else {
            bienPrestataire = bienPrestataireRepo.findById(id).get();
        }

        return bienPrestataire;
    }

    @Override
    public List<BienPrestataire> chercherTousLesElements() {
        return bienPrestataireRepo.findAll();
    }

    @Override
    public void supprimerParId(Long id) throws ProjetMaisonExceptions {
        Optional<BienPrestataire> op = bienPrestataireRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_INCONNU);
        } else {
            bienPrestataireRepo.deleteById(id);
        }
    }
}
