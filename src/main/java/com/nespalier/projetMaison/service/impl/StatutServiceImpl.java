package com.nespalier.projetMaison.service.impl;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.model.Statut;
import com.nespalier.projetMaison.repo.IStatutRepo;
import com.nespalier.projetMaison.service.IStatutService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StatutServiceImpl implements IStatutService {

    @Autowired
    IStatutRepo statutRepo;

    Statut statut = new Statut();

    @Override
    public Statut creerModifier(Statut objetAcreer) throws ProjetMaisonExceptions {

        if (StringUtils.isNoneEmpty(objetAcreer.getLibelle())) {
            statut = statutRepo.save(objetAcreer);
        } else {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.STATUT_LIBELLE_VIDE);
        }
        return statut;
    }

    @Override
    public Statut trouveParClef(Long id) throws ProjetMaisonExceptions {

        Optional<Statut> op = statutRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.STATUT_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.STATUT_INCONNU);
        } else {
            statut = statutRepo.findById(id).get();
        }

        return statut;
    }

    @Override
    public List<Statut> chercherTousLesElements() {

        return statutRepo.findAll(Sort.by(Sort.Direction.ASC, "libelle"));
    }

    @Override
    public void supprimerParId(Long id) throws ProjetMaisonExceptions {

        Optional<Statut> op = statutRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.STATUT_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.STATUT_INCONNU);
        } else {
            statutRepo.deleteById(id);
        }
    }

}
