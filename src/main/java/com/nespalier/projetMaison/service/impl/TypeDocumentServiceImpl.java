package com.nespalier.projetMaison.service.impl;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.model.TypeDocument;
import com.nespalier.projetMaison.repo.ITypeDocumentRepo;
import com.nespalier.projetMaison.service.ITypeDocumentService;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TypeDocumentServiceImpl implements ITypeDocumentService {

    @Autowired
    ITypeDocumentRepo typeDocRepo;

    TypeDocument typeDocument = new TypeDocument();

    @Override
    public TypeDocument creerModifier(TypeDocument objetAcreer) throws ProjetMaisonExceptions {
        TypeDocument typeDocument = new TypeDocument();
        if (StringUtils.isNotEmpty(objetAcreer.getLibelle())) {
            typeDocument = typeDocRepo.save(objetAcreer);
        } else {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.LIBELLE_TYPE_DOCUMENT_VIDE);
        }
        return typeDocument;
    }

    @Override
    public TypeDocument trouveParClef(Long id) throws ProjetMaisonExceptions {
        Optional<TypeDocument> op = typeDocRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.TYPE_DOCUMENT_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.TYPE_DOCUMENT_INCONNU);
        } else {
            typeDocument = typeDocRepo.findById(id).get();
        }

        return typeDocument;
    }

    @Override
    public List<TypeDocument> chercherTousLesElements() {

        return typeDocRepo.findAll(Sort.by(Sort.Direction.ASC, "libelle"));
    }

    @Override
    public void supprimerParId(Long id) throws ProjetMaisonExceptions {
        Optional<TypeDocument> op = typeDocRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.TYPE_DOCUMENT_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.TYPE_DOCUMENT_INCONNU);
        } else {
            typeDocRepo.deleteById(id);
        }
    }
}
