package com.nespalier.projetMaison.service.impl;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.commons.service.DocumentService;
import com.nespalier.projetMaison.model.Document;
import com.nespalier.projetMaison.model.Statut;
import com.nespalier.projetMaison.repo.IDocumentRepo;
import com.nespalier.projetMaison.service.IDocumentService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Service
public class DocumentServiceImpl implements IDocumentService {

    @Autowired
    IDocumentRepo documentRepo;

    Document document = new Document();

    @Override
    public Document creerModifier(Document objetAcreer) throws ProjetMaisonExceptions {
        if (ObjectUtils.isNotEmpty(objetAcreer)) {
            document = documentRepo.save(objetAcreer);
        }else {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.DOCUMENT_VIDE);
        }
        return document;
    }

    @Override
    public Document trouveParClef(Long id) throws ProjetMaisonExceptions {

        Optional<Document> op = documentRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.DOCUMENT_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.DOCUMENT_INCONNU);
        } else {
            document = documentRepo.findById(id).get();
        }

        return document;
    }

    @Override
    public List<Document> chercherTousLesElements() {

        return documentRepo.findAll(Sort.by(Sort.Direction.ASC, "documentPath"));
    }

    @Override
    public void supprimerParId(Long id) throws ProjetMaisonExceptions {
        Optional<Document> op = documentRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.DOCUMENT_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.DOCUMENT_INCONNU);
        } else {
            documentRepo.deleteById(id);
        }
    }

    @Override
    public Document ajoutPathDocument(Long id, MultipartFile file) throws ProjetMaisonExceptions {
        Document doc = trouveParClef(id);
        if(ObjectUtils.isNotEmpty(file) && ObjectUtils.isNotEmpty(doc)){
            doc.setDocumentPath(file.getOriginalFilename());
            doc = documentRepo.save(doc);
        }
        return doc;
    }
}
