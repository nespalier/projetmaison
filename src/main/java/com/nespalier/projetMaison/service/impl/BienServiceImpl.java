package com.nespalier.projetMaison.service.impl;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.model.Bien;
import com.nespalier.projetMaison.repo.IBienRepo;
import com.nespalier.projetMaison.service.IBienService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BienServiceImpl implements IBienService {

    @Autowired
    IBienRepo bienRepo;

    Bien bien = new Bien();

    @Override
    public Bien creerModifier(Bien bienAcreerOuModifier) throws ProjetMaisonExceptions {
        if (ObjectUtils.isNotEmpty(bienAcreerOuModifier)) {
            bien = bienRepo.save(bienAcreerOuModifier);
        } else {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_VIDE);
        }
        return bien;
    }

    @Override
    public Bien trouveParClef(Long id) throws ProjetMaisonExceptions {
        Optional<Bien> op = bienRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_INCONNU);
        } else {
            bien = bienRepo.findById(id).get();
        }

        return bien;
    }

    @Override
    public List<Bien> chercherTousLesElements() {

        return bienRepo.findAll(Sort.by(Sort.Direction.ASC, "libelle"));
    }

    @Override
    public void supprimerParId(Long id) throws ProjetMaisonExceptions {

        Optional<Bien> op = bienRepo.findById(id);
        if (null == id) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_INCONNU);
        } else if (!op.isPresent()) {
            throw new ProjetMaisonExceptions(ProjetMaisonConstante.BIEN_INCONNU);
        } else {
            bienRepo.deleteById(id);
        }
    }

}
