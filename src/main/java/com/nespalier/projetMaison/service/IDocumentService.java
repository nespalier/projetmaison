package com.nespalier.projetMaison.service;

import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.commons.interfaces.BaseCrud;
import com.nespalier.projetMaison.model.Document;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface IDocumentService extends BaseCrud<Document> {

    Document ajoutPathDocument(Long id,MultipartFile file) throws ProjetMaisonExceptions;

}
