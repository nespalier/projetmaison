package com.nespalier.projetMaison.service;

import com.nespalier.projetMaison.commons.interfaces.BaseCrud;
import com.nespalier.projetMaison.model.BienPrestataire;
import com.nespalier.projetMaison.model.Document;

public interface IBienPrestataireService extends BaseCrud<BienPrestataire> {
}
