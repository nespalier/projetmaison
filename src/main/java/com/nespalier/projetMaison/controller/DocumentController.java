package com.nespalier.projetMaison.controller;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.commons.interfaces.BaseController;
import com.nespalier.projetMaison.model.Document;
import com.nespalier.projetMaison.model.Statut;
import com.nespalier.projetMaison.service.IDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(ProjetMaisonConstante.PATH + "/document")
public class DocumentController implements BaseController<Document> {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatutController.class);
    @Autowired
    IDocumentService documentService;

    @Override
    public ResponseEntity<Document> creerModifier(Document document) throws ProjetMaisonExceptions {
        try {
            LOGGER.info("Create document {}", document.toString());
            Document response = documentService.creerModifier(document);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<Document> trouveParClef(Long id) throws ProjetMaisonExceptions {
        try {
            Document response = documentService.trouveParClef(id);
            LOGGER.info("Document trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<Void> supprimerParId(Long id) throws ProjetMaisonExceptions {
        try {
            Document documentSupprime = documentService.trouveParClef(id);
            documentService.supprimerParId(id);
            LOGGER.info("Document supprimé {}", documentSupprime.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<List<Document>> chercherTousLesElements() {
        List<Document> response = documentService.chercherTousLesElements();
        return ResponseEntity.status(HttpStatus.OK).body(response) ;
    }

    @PostMapping(path = "/{id}")
    public ResponseEntity<Document> ajoutPathDocument(@PathVariable(value = "id") Long id, @RequestParam("docImport") MultipartFile file) throws IOException {


        try {
            Document response = documentService.trouveParClef(id);
            response = documentService.ajoutPathDocument(id,file);
            LOGGER.info("Document trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
