package com.nespalier.projetMaison.controller;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.commons.interfaces.BaseController;
import com.nespalier.projetMaison.model.Document;
import com.nespalier.projetMaison.model.Prestataire;
import com.nespalier.projetMaison.service.IPrestataireService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(ProjetMaisonConstante.PATH + "/prestataire")
public class PrestataireController implements BaseController<Prestataire> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrestataireController.class);
    @Autowired
    IPrestataireService prestataireService;

    @Override
    public ResponseEntity<Prestataire> creerModifier(Prestataire prestataire) throws ProjetMaisonExceptions {
        try {
            LOGGER.info("Create prestataire {}", prestataire.toString());
            Prestataire response = prestataireService.creerModifier(prestataire);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<Prestataire> trouveParClef(Long id) throws ProjetMaisonExceptions {
        try {
            Prestataire response = prestataireService.trouveParClef(id);
            LOGGER.info("Prestataire trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<Void> supprimerParId(Long id) throws ProjetMaisonExceptions {
        try {
            Prestataire prestataireSupprime = prestataireService.trouveParClef(id);
            prestataireService.supprimerParId(id);
            LOGGER.info("Prestataire supprimé {}", prestataireSupprime.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<List<Prestataire>> chercherTousLesElements() {
        List<Prestataire> response = prestataireService.chercherTousLesElements();
        return ResponseEntity.status(HttpStatus.OK).body(response) ;
    }
}
