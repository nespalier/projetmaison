package com.nespalier.projetMaison.controller;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.commons.interfaces.BaseController;
import com.nespalier.projetMaison.model.Statut;
import com.nespalier.projetMaison.model.TypeDocument;
import com.nespalier.projetMaison.service.ITypeDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(ProjetMaisonConstante.PATH + "/typeDocument")
public class TypeDocumentController implements BaseController<TypeDocument> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TypeDocumentController.class);

    @Autowired
    ITypeDocumentService typeDocService;

    @Override
    public ResponseEntity<TypeDocument> creerModifier(TypeDocument typeDocument) throws ProjetMaisonExceptions {
        try {
            LOGGER.info("Create type document {}", typeDocument.toString());
            TypeDocument response = typeDocService.creerModifier(typeDocument);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<TypeDocument> trouveParClef(Long id) throws ProjetMaisonExceptions {
        try {
            TypeDocument response = typeDocService.trouveParClef(id);
            LOGGER.info("Type document trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<Void> supprimerParId(Long id) throws ProjetMaisonExceptions {
        try {
            TypeDocument typeDocSupprime = typeDocService.trouveParClef(id);
            typeDocService.supprimerParId(id);
            LOGGER.info("Type document supprimé {}", typeDocSupprime.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<List<TypeDocument>> chercherTousLesElements() {
        List<TypeDocument> response = typeDocService.chercherTousLesElements();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
