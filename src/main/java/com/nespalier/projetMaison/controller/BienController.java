package com.nespalier.projetMaison.controller;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.commons.interfaces.BaseController;
import com.nespalier.projetMaison.model.Bien;
import com.nespalier.projetMaison.model.Statut;
import com.nespalier.projetMaison.service.IBienService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(ProjetMaisonConstante.PATH+"/bien")
public class BienController implements BaseController<Bien> {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatutController.class);
    @Autowired
    IBienService bienService;

    @Override
    public ResponseEntity<Bien> creerModifier(Bien bien) throws ProjetMaisonExceptions {
        try {
            LOGGER.info("Create bien {}", bien.toString());
            Bien response = bienService.creerModifier(bien);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public  ResponseEntity<Bien> trouveParClef(Long id) throws ProjetMaisonExceptions {
        try {
            Bien response = bienService.trouveParClef(id);
            LOGGER.info("bien trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<Void> supprimerParId(Long id) throws ProjetMaisonExceptions {
        try {
            Bien bienSupprime = bienService.trouveParClef(id);
            bienService.supprimerParId(id);
            LOGGER.info("bien supprimé {}", bienSupprime.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public  ResponseEntity<List<Bien>> chercherTousLesElements() {
        List<Bien> response = bienService.chercherTousLesElements();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


}
