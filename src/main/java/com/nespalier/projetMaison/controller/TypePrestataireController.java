package com.nespalier.projetMaison.controller;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.commons.interfaces.BaseController;
import com.nespalier.projetMaison.model.TypeDocument;
import com.nespalier.projetMaison.model.TypePrestataire;
import com.nespalier.projetMaison.service.ITypeDocumentService;
import com.nespalier.projetMaison.service.ITypePrestataireService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(ProjetMaisonConstante.PATH + "/typePrestataire")
public class TypePrestataireController implements BaseController<TypePrestataire> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TypePrestataireController.class);

    @Autowired
    ITypePrestataireService typePrestaService;


    @Override
    public ResponseEntity<TypePrestataire> creerModifier(TypePrestataire typePrestataire) throws ProjetMaisonExceptions {
        try {
            LOGGER.info("Create type prestataire {}", typePrestataire.toString());
            TypePrestataire response = typePrestaService.creerModifier(typePrestataire);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<TypePrestataire> trouveParClef(Long id) throws ProjetMaisonExceptions {
        try {
            TypePrestataire response = typePrestaService.trouveParClef(id);
            LOGGER.info("Type prestataire trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<Void> supprimerParId(Long id) throws ProjetMaisonExceptions {
        try {
            TypePrestataire typePrestaSupprime = typePrestaService.trouveParClef(id);
            typePrestaService.supprimerParId(id);
            LOGGER.info("Type prestataire supprimé {}", typePrestaSupprime.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<List<TypePrestataire>> chercherTousLesElements() {
        List<TypePrestataire> response = typePrestaService.chercherTousLesElements();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
