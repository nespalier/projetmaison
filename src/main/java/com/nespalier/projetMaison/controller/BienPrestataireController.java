package com.nespalier.projetMaison.controller;

import com.nespalier.projetMaison.commons.constantes.ProjetMaisonConstante;
import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.commons.interfaces.BaseController;
import com.nespalier.projetMaison.model.Bien;
import com.nespalier.projetMaison.model.BienPrestataire;
import com.nespalier.projetMaison.service.IBienPrestataireService;
import com.nespalier.projetMaison.service.IBienService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(ProjetMaisonConstante.PATH+"/bienPrestataire")
public class BienPrestataireController implements BaseController<BienPrestataire> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BienPrestataireController.class);
    @Autowired
    IBienPrestataireService bienPrestaService;

    @Override
    public ResponseEntity<BienPrestataire> creerModifier(BienPrestataire bienPrestataire) throws ProjetMaisonExceptions {
        try {
            LOGGER.info("Create bienPrestataire {}", bienPrestataire.toString());
            BienPrestataire response = bienPrestaService.creerModifier(bienPrestataire);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public  ResponseEntity<BienPrestataire> trouveParClef(Long id) throws ProjetMaisonExceptions {
        try {
            BienPrestataire response = bienPrestaService.trouveParClef(id);
            LOGGER.info("bienPrestataire trouvé {}", response.toString());
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<Void> supprimerParId(Long id) throws ProjetMaisonExceptions {
        try {
            BienPrestataire bienPrestataireSupprime = bienPrestaService.trouveParClef(id);
            bienPrestaService.supprimerParId(id);
            LOGGER.info("bienPrestataire supprimé {}", bienPrestataireSupprime.toString());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ProjetMaisonExceptions e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public  ResponseEntity<List<BienPrestataire>> chercherTousLesElements() {
        List<BienPrestataire> response = bienPrestaService.chercherTousLesElements();
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


}
