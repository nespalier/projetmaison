package com.nespalier.projetMaison.model;

import javax.persistence.*;

@Entity
public class Prestataire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_typePrestataire")
    private TypePrestataire type;

    private int numTel;

    private String mail;

    private String contact;

    private String description;

    public Prestataire() {
    }

    public Prestataire(TypePrestataire type, int numTel, String mail, String contact) {
        this.type = type;
        this.numTel = numTel;
        this.mail = mail;
        this.contact = contact;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypePrestataire getType() {
        return type;
    }

    public void setType(TypePrestataire type) {
        this.type = type;
    }

    public int getNumTel() {
        return numTel;
    }

    public void setNumTel(int numTel) {
        this.numTel = numTel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Prestataire{" +
                "id=" + id +
                ", type=" + type +
                ", numTel=" + numTel +
                ", mail='" + mail + '\'' +
                ", contact='" + contact + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
