package com.nespalier.projetMaison.model;

import javax.persistence.*;

@Entity
public class BienPrestataire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_prestataire")
    private Prestataire prestataire;

    @ManyToOne
    @JoinColumn(name = "id_bien")
    private Bien bien;

    private Long montant = 0L;

    public BienPrestataire() {
    }

    public BienPrestataire(Prestataire prestataire, Bien bien, Long montant) {
        this.prestataire = prestataire;
        this.bien = bien;
        this.montant = montant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Prestataire getPrestataire() {
        return prestataire;
    }

    public void setPrestataire(Prestataire prestataire) {
        this.prestataire = prestataire;
    }

    public Bien getBien() {
        return bien;
    }

    public void setBien(Bien bien) {
        this.bien = bien;
    }

    public Long getMontant() {
        return montant;
    }

    public void setMontant(Long montant) {
        this.montant = montant;
    }

    @Override
    public String toString() {
        return "BienPrestataire{" +
                "id=" + id +
                ", prestataire=" + prestataire +
                ", bien=" + bien +
                ", montant=" + montant +
                '}';
    }
}
