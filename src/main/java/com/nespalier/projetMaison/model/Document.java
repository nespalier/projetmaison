package com.nespalier.projetMaison.model;

import javax.persistence.*;

@Entity
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_typeDocument")
    private TypeDocument type;

    @ManyToOne
    @JoinColumn(name = "id_statut")
    private Statut statut;

    @Column(name="path")
    private String documentPath;

    private String description;

    public Document() {
    }

    public Document(TypeDocument type, Statut statut, String documentPath) {
        this.type = type;
        this.statut = statut;
        this.documentPath = documentPath;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeDocument getType() {
        return type;
    }

    public void setType(TypeDocument type) {
        this.type = type;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", type=" + type +
                ", statut=" + statut +
                ", documentPath='" + documentPath + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
