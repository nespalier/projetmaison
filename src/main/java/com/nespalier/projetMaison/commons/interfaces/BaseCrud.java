package com.nespalier.projetMaison.commons.interfaces;

import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;

import java.util.List;

public interface BaseCrud<T> {

    T creerModifier(T objetAcreer) throws ProjetMaisonExceptions;

    T trouveParClef(Long id) throws ProjetMaisonExceptions;

    List<T> chercherTousLesElements();

    void supprimerParId(Long id) throws ProjetMaisonExceptions;

}
