package com.nespalier.projetMaison.commons.interfaces;

import com.nespalier.projetMaison.commons.exceptions.ProjetMaisonExceptions;
import com.nespalier.projetMaison.controller.StatutController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface BaseController<T> {


    @PostMapping
    public ResponseEntity<T> creerModifier(@RequestBody T t) throws ProjetMaisonExceptions;

    @GetMapping(value = {"/{id}"})
    public ResponseEntity<T> trouveParClef(@PathVariable("id") Long id) throws ProjetMaisonExceptions;

    @DeleteMapping(value = {"/{id}"})
    public ResponseEntity<Void> supprimerParId(@PathVariable("id") Long id) throws ProjetMaisonExceptions;

    @GetMapping
    public ResponseEntity<List<T>> chercherTousLesElements();

}
