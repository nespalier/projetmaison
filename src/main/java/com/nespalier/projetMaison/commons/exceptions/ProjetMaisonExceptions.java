package com.nespalier.projetMaison.commons.exceptions;

public class ProjetMaisonExceptions extends Exception {

    public ProjetMaisonExceptions(String message) {
        super(message);
    }
}
