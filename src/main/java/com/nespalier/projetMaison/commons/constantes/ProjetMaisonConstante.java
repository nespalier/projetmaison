package com.nespalier.projetMaison.commons.constantes;

public class ProjetMaisonConstante {

    public static final String PATH = "/maison-0.0.1-SNAPSHOT";

    // Message d'erreur
    public static final String STATUT_INCONNU = "Le statut renseigné n'est pas connu en base.";
    public static final String TYPE_DOCUMENT_INCONNU = "Le type de document renseigné n'est pas connu en base.";
    public static final String TYPE_PRESTATIRE_INCONNU = "Le type de prestataire renseigné n'est pas connu en base.";
    public static final String DOCUMENT_INCONNU = "Le document renseigné n'est pas connu en base.";
    public static final String BIEN_INCONNU = "Le bien renseigné n'est pas connu en base.";
    public static final String PRESTATAIRE_INCONNU = "Le prestataire renseigné n'est pas connu en base.";
    public static final String STATUT_LIBELLE_VIDE = "Le libelle du statut est vide.";
    public static final String LIBELLE_TYPE_DOCUMENT_VIDE = "Le type de docuemnt est vide.";
    public static final String LIBELLE_TYPE_PRESTATAIRE_VIDE = "Le libellé du type de prestataire est vide.";
    public static final String DOCUMENT_VIDE = "Le docuemnt est vide.";
    public static final String BIEN_VIDE = "Le bien est vide.";
    public static final String PRESTATAIRE_VIDE = "Le prestataire est vide.";
}
